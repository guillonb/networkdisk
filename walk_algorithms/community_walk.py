from random import choice
def community_walk(Graph, node, miss_restart=4, max_walk_nb=10, max_size=2000, verbose=False):
    comm = {node:1}
    path_length = []
    for _ in range(max_walk_nb):
         if len(comm) > max_size:
             break
         x = node
         miss = 0
         path = []
         l = 0
         while miss < miss_restart:
             if verbose:
                 print(path)
             path.append(x)
             y = choice(list(Graph[x]))
             l += 1
             if y not in comm:
                 miss+= 1
             else:
                 miss = 0
                 for t in path:
                     comm[t] = comm.get(t, 0) + 1
                 path = []
             x = y
         path_length.append(l)
    return comm
