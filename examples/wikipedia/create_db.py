#! /usr/bin/python3
import sys, os
cdir = os.path.dirname(sys.argv[0])
sys.path.append(f"{cdir}/../")
import sqlite3
import bz2
# import xml.sax
from lxml import etree
import re
import time
import json
import time
sys.path.append(f"{cdir}/../")
import networkdisk as nd
import collections
stripws = re.compile("\s+", re.MULTILINE|re.DOTALL)
cleanr  = re.compile("(<.*?>)|(\{.*?\})|(\[\[File:.*\]\])|(\[\[[^\|]*?\|)", re.MULTILINE|re.DOTALL)
extract_word = re.compile("\w[\w,!?\-.\s]+", re.MULTILINE|re.DOTALL)
extract_wikilink = re.compile("\[\[([^|\]]*)")
cleanpar = re.compile("[\[\]\|]", re.MULTILINE|re.DOTALL)
txt_limit = 100000
commit_factor = 10**3 
ns = "{http://www.mediawiki.org/xml/export-0.10/}"
rprint = lambda e:print(e, " "*30)

def cleanhtml(raw_html):
    cleantext = raw_html
    cleantext = cleanr.sub('', cleantext)
    cleantext = stripws.sub(' ', cleantext)
    cleantext = cleanpar.sub('', cleantext)
    cleantext = extract_word.findall(cleantext)
    cleantext = " ".join(filter(bool, cleantext))[:txt_limit]
    return cleantext

page_class = collections.namedtuple("page_class", ("title", "links", "redirect", "content"))


def create_nd_graph(path):
    inserted_edges_glob = 0
    G = nd.sqlite.DiGraph(db=f"{path}wiki.db", sql_logger=False, name="wiki", insert_schema=True)
    i = 0
    Tinit = T = time.time_ns()
    fsize = os.stat(f"{path}wiki.xml.bz2").st_size
    f = bz2.open(f"{path}wiki.xml.bz2")
    context = etree.iterparse(f, tag=f"{ns}page") # will stop at each tag page
    def load_graph(pages):
        T2 = time.time_ns()
        edgelist = [(page.title, link) for page in pages for link in page.links]
        T2b = time.time_ns()
        edges_nb = len(edgelist)
        G.add_edges_from(edgelist)
        T3 = time.time_ns()
        G.add_nodes_from([(page.title, {"content":page.content, "is_redirect":page.redirect}) for page in pages])
        T4 = time.time_ns()
        return T2, T2b, T3, T4, edges_nb
    pages = []
    for _, page in context:
        T0 = time.time_ns()
        title = page.find(f"{ns}title").text
        title = title.strip() if title else ""
        content = page.find(f"{ns}revision/{ns}text").text
        content = content.strip() if content else ""
        links = extract_wikilink.findall(content)
        redirect = page.find(f"{ns}redirect")
        if redirect is not None:
            redirect = redirect.attrib["title"]
        pages.append(page_class(title=title.strip(), links=list(map(str.strip, links)), content=cleanhtml(content), redirect=redirect))
        if len(pages) > commit_factor:
            T2, T2b, T3, T4, edges_nb = load_graph(pages)
            inserted_edges_glob += edges_nb
            if i:
                print("\033[A"*12)
            i+= 1
            rprint(f"{commit_factor*i} pages inserted")
            rprint(f"Parsing:\t\t{(T2-T)/10**9:.1f}s")
            rprint(f"Total nd:\t\t{(T4-T2)/10**9:.1f}s")
            rprint(f"\tAdd edges:\t{(T3-T2)/10**9:.1f}s")
            rprint(f"\t\t{edges_nb} new edges, {inserted_edges_glob} total")
            rprint(f"\tmaterialisation:\t{(T2b-T2)/10**9:.1f}s")
            rprint(f"\tAdd nodes:\t{(T4-T3)/10**9:.1f}s")
            rprint(f"Total: {(i*commit_factor*10**9)/(T4-Tinit):.1f} Pages/s")
            rprint(f"Last loop: {commit_factor*10**9/(T4-T):.1f} Page/s") 
            rprint(f"File cursor: {1000*f.tell()/fsize:.2f}‰ ")
            rprint(f"Time remaining {((T4 - Tinit)*fsize/f.tell())/(60*10**9):0.1f} minutes")
            T = T4
            pages = []
            inserted_edges_loc = 0
        page.clear() # free memory
    load_graph()
    G.helper.db.close()

if __name__ == "__main__":
    datapath = sys.argv[1] if len(sys.argv) > 1 else "data"
    dbpath = f"{datapath}/"
    create_nd_graph(dbpath)
