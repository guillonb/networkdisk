#!/usr/bin/python3
import sys, os, json, datetime
cdir = os.path.dirname(sys.argv[0])
sys.path.append(f"{cdir}/../")
import networkdisk as nd

read_block = 10**7
if len(sys.argv)>1:
	datapath = sys.argv[1]
else:
	datapath = "dblp"
dbpath = f"{datapath}/dblp.db"

if os.path.exists(dbpath):
	os.remove(dbpath)
#schema = nd.sqlite.dialect.schemata.ungraph_unsplitted()
G = nd.sqlite.Graph(db=dbpath, name="dblp", insert_schema=True, sql_logger=False, schema=dict(node="INT"))
G.graph["date"] = str(datetime.date.today())
G.drop_index()
#with open(datapath+"/edges") as f:
#	edges = filter(lambda e:"\t" in e, f)
#	edges = map(lambda e:e.strip().split("\t"), edges)
#	edges = map(lambda e: (int(e[0]), int(e[1])), edges)
#	G.add_edges_from(list(edges)) #<1go, should hold in memory.
print("starting with nodes insertions")
with open(datapath+"/nodes") as f:
	remain = ""
	fsize = os.stat(datapath+"/nodes").st_size
	rsize = 0
	while True:
		x = remain + f.read(read_block)
		rsize += read_block
		if not x:
			break
		content, remain = x.rsplit("\n", maxsplit=1)
		data = list(map(lambda e:(e.pop("index"), e), map(json.loads, content.split("\n"))))
		G.add_nodes_from(data)
		print(f"{100*rsize/fsize:0.2f}%  ({rsize/10**6:0.2f}Mo done)", end="\r", flush=True)
print()
print("Done")

print("Starting edges insertion")
with open(datapath+"/edges") as f:
	edges = filter(lambda e:"\t" in e, f)
	edges = map(lambda e:e.strip().split("\t"), edges)
	edges = map(lambda e: (int(e[0]), int(e[1])), edges)
	G.add_edges_from(edges)
print()
print("Over, reindexing...")
G.reindex()
print(f"Graph saved at {dbpath}")
