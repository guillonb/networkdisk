#!/bin/bash

#pdir: the program dir (where to find subscripts)
pdir="$(dirname $0)"
pdir="${pdir:-.}"
pdir="${pdir%/}"

#option flags (c.f. help)
clean=true
force=false
interactive=false
master=false
parse=true
while getopts "Cfhis" opt
do
	case $opt in
		C) clean=false;;
		f) force=true;;
		h) echo "Usage: $0 [-Cfhisp] [directory]"
			echo
			echo " -C  do not clean downloaded files after import"
			echo " -f  force import (overwrite previously imported DBLP data)."
			echo " -h  show this help and exit. "
			echo " -i  load the graph in interactive ipython, after import."
			echo " -s  save the graph schema in master table."
			echo " -p  do not parse the xml files. Usefull when file already parsed."
			exit 0;;
		i) interactive=true;;
		s) master=true;;
		p) parse=false
	esac
done
shift $(($OPTIND-1))

#dirpath: the target directory (where to download and import)
dirpath="${1:-.}"
[ ! -d "$dirpath" ] && echo "$dirpath: not a directory" >&2 && exit 1
if [ -z "$dirpath" ]
then
	dirpath="${dirpath%/}/$(date "+%y%m%d")"
fi

#Download and import
if $force || [ ! -f "$dirpath/dblp.db" ]
then
	echo "Caution, the script will download large files"
	mkdir -p "$dirpath"
	if [ ! -f "$dirpath/dblp.xml" ]; then
		wget -P "$dirpath" https://dblp.uni-trier.de/xml/dblp.dtd
		wget -P "$dirpath" https://dblp.uni-trier.de/xml/dblp.xml.gz
		gunzip "$dirpath/dblp.xml.gz"
	fi
	if $parse
	then
		echo "Parsing of DBLP"
		eval "$pdir/parsing_dblp.py" "$dirpath"
		echo "Graph creation"
		eval "$pdir/create_nd_graph.py" "$dirpath"
	else
		echo "File already parsed"
	fi
	if $clean
	then
		echo "Cleaning downloaded files"
		rm "$dirpath/nodes"
		rm "$dirpath/edges"
	fi
else
	echo "DBLP graph found in $dirpath/dblp.db (use -f to overwrite)."
fi

#Insert Python schema in master table
pyopengraph="G = nd.sqlite.Graph(db='$dirpath/dblp.db', name='dblp')"
if $master
then
	python3 -c "import networkdisk as nd; ${pyopengraph%)}, insert_schema=True)"\
		&& pyopengraph="G = nd.sqlite.Graph(db='$dirpath/dblp.db')"
fi

#Launch interactive mode
if $interactive
then
	ipython3 -c "import networkdisk as nd; $pyopengraph; print('Play with the DBLP graph G.')" -i
else
	echo "Within Python, use:"
	echo ">>> import networkdisk as nd"
	echo ">>> $pyopengraph"
fi

