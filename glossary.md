
+ SQL
	+ constraint: an SQL **column** or **table** constraint (e.g., `NOT NULL`, `CHECK min<=max`)
	+ condition: an object that represents a **query** condition (e.g., `min<=max AND goal<=max`, `name IS NOT NULL`)
	+ query: an object that can be executed by an SQLHelper.
	+ free_columns: the iterable of columns accessible from the outside of a query
		+	InsertQuery, UpdateQuery, DeleteQuery, CreateQuery have no free_columns
	+ binded_columns: the iterable of columns accessible from inside a query
		+	SchemaTable, SchemaView have no binded_columns
+ TupleDict: an abstraction of fixed-depth recursive dictionary
	+ depth:
	+ height:
	+ rows: a tuple `r` such that `r[:-1]` is a tuplekey and `r[-1]` is the associated tuplevalue
	+ tuplekey: the tuple of key-component in TupleDict
	+ tuplevalue:

