Finding edges and nodes
=======================

One of the main key features to use
a (Di)Graph holding data as a database, is the ability
to quickly retrieve nodes and edges.

Performance on this subject depends on the backend indexation
ability. This tutorial will focus on the SQLite implementation.

TODO: update for PostgreSQL when ready

We first define an simple graph with data on both nodes and edges.

	>>> G = nd.sqlite.DiGraph()
	>>> G.add_node(0, foo="bar")
	>>> G.add_node(1, foo="car")
	>>> G.add_node(2, value=23)
	>>> G.add_node(3, foo="bar", value=42)
	>>> G.add_node(4, value=54, foo="bir")
	>>> G.add_edges_from([(0, 1, {"w":0}), (1, 2, {"w":1}), (0, 2), (3, 4, {"color":"black"})])

Simple Queries
^^^^^^^^^^^^^^

Finding all nodes having a `foo` as key:

	>>> sorted(G.find_all_nodes("foo"))
	[0, 1, 3, 4]

Finding all nodes having `foo` as key with value `bar`

	>>> sorted(G.find_all_nodes("foo", foo="bar"))
	[0, 3]

Finding all nodes that either do not have "foo" as key, or have "foo" as key with associated value "bar".

	>>> sorted(G.find_all_nodes(foo="bar"))
	[0, 2, 3]

The same kind of methods exist for edges searching.

	>>> sorted(G.find_all_edges(w=0))
	[(0, 1), (0, 2), (3, 4)]


More complex queries
^^^^^^^^^^^^^^^^^^^^

It is possible to constrain some value with semantic conditions.
We need to provide some SQL condition to the SQL column coding the value.
SQL conditions are encoded easily through operation on columns.

The condition builder is exposed on the topmost graphs classes through
the :py:meth:`networkdisk.sql.Graph.nodes_satisfying_condition` and
:py:meth:`networkdisk.sql.Graph.edges_satisfying_conditions`.


Finding all nodes whose `value` is greater than `30`

	>>> sorted(G.find_all_nodes("value", value=lambda c:c.gt(30)))
	[3, 4]


It is possible to perform SQL queries as well

	>>> sorted(G.find_all_nodes("foo", foo=lambda c: c.like("b_r")))
	[0, 3, 4]

