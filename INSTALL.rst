Install
=======

NetworkDisk requires Python3.6.

Below we assume you have the default Python environment already configured on
your computer and you intend to install ``networkx`` inside of it.  If you want
to create and work with Python virtual environments, please follow instructions
on `venv <https://docs.python.org/3/library/venv.html>`_ and `virtual
environments <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_.

First, make sure you have the latest version of ``pip`` (the Python package manager)
installed. If you do not, refer to the `Pip documentation
<https://pip.pypa.io/en/stable/installing/>`_ and install ``pip`` first.


Dependencies
------------

NetworkDisk depends on the following packages:

- `networkx <https://networkx.org/>`_
- `termcolor <https://pypi.org/project/termcolor/>`_ for colored output of loggers


Installing with dependencies
----------------------------

To install ``NetworkDisk`` with dependencies using ``pypi``, do::

  $ python3 -m pip install networkdisk

To install ``NetworkDisk`` with dependencies from source, do, from the package root directory::

	$ python3 -m pip install .

To install optional dependencies to run tests or generate documentation do::

	$ python3 -m pip install .[test]
	$ python3 -m pip install .[doc]

Testing
-------

NetworkDisk uses the Python ``pytest`` testing package.  You can learn more
about pytest on their `homepage <https://pytest.org>`_.
It relies on NetworkX tests that are executed with NetworkDisk objects.

Test a source distribution
^^^^^^^^^^^^^^^^^^^^^^^^^^

You can test the complete package from the unpacked source directory with::

    $ pytest networkdisk

Test an installed package
^^^^^^^^^^^^^^^^^^^^^^^^^

From a shell command prompt you can test the installed package with::

   $ pytest --pyargs networkdisk

If you have a file-based (not a Python egg) installation you can test the
installed package with::

    >>> import networkdisk as nd
    >>> nd.test()

or::

    $ python3 -c "import networkdisk as nd; nd.test()"


Building the doc
----------------

To build the documentation locally, you first need to install
`Sphinx <https://www.sphinx-doc.org/en/master/>`_ and the various `Sphinx`
extensions used.
It can be done easily with::

	$ python3 -m pip -r install doc/requirements.txt

Then building the documentation::

	$ make -C doc/ html

Testing the examples in the documentation::

	$ make -C doc/ doctest

The documentation will be build in the directory `doc/_build/html`.
