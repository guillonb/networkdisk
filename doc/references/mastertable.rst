The `MasterTable`
=================

.. currentmodule:: networkdisk.sql.master

.. autofunction:: mastertable

We can perform various graph manipulations using this class:

.. autoclass:: MasterGraphs

.. autosummary::
	:toctree: generated/

	MasterGraphs.delete_graph
	MasterGraphs.list_graphs
	MasterGraphs.load_graph
	MasterGraphs.save_graph

.. testcleanup::
	import os
	os.remove("/tmp/mydb.db")
	os.remove("/tmp/master.db")
