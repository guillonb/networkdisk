Introduction
============

The main entry point to NetworkDisk are the Graph and DiGraph classes.

.. currentmodule:: networkdisk.sql

Graph
-----

.. autoclass:: Graph
    :members:
    :special-members: __create_graph_init__, __load_graph_init__

.. autoclass:: networkdisk.sqlite.Graph
    :members:

DiGraph
-------

.. autoclass:: DiGraph
    :members:

.. autoclass:: networkdisk.sqlite.DiGraph
    :members:

