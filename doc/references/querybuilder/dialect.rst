.. _dialect:

===========
SQL Dialect
===========

Overview
========

.. testsetup:: *

   from networkdisk.sql.dialect import Dialect

Dialect class
=============

.. automodule:: networkdisk.sql.dialect

.. currentmodule:: networkdisk.sql.dialect


Methods
=======

.. autoclass:: Dialect

	.. automethod:: register


Typical usage
=============

TODO
