import os, tempfile, random, sqlite3
import networkdisk as nd
import networkx as nx
import matplotlib.pyplot as plt

testdir = f"/tmp/networkdisk/doctests"
nd.sql.Graph.tempfile_dir = tempdir = f"{testdir}/temp"
nd.sql.Graph.autofile_dir = autodir = f"{testdir}/auto"

dbdirs = [testdir, tempdir, autodir]

random.seed(42)
randint = random.randint
dblp_path = "../examples/test/dblp.db"
dblp_path = dblp_path if os.path.exists(dblp_path) else None
wiki_path = "../examples/test/wikipedia.db"
wiki_path = wiki_path if os.path.exists(wiki_path) else None
databases = ["/tmp/mydb.db", "/tmp/myotherdb.db", "/tmp/master.db"]

for d in dbdirs:
	if os.path.isdir(d):
		#for cleaning
		databases.extend(filter(os.path.isfile, os.listdir(d)))
	else:
		os.makedirs(d, exist_ok=True)

for db in databases:
	try:
		os.remove(db)
	except FileNotFoundError:
		pass

