In case of LEFT JOIN, we use the [known optimization](https://www.sqlite.org/optoverview.html#joins)
of pushing the condition within the ON-clause as much as possible, when semantically equivalent.
