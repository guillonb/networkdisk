# coding: utf-8
from networkdisk.sql import *
T1 = Table("person")
T1.add_column("id", primarykey=True, sqltype="INT")
T1.add_column("name", sqltype="TEXT")
T1.add_column("key", sqltype="TEXT")
T1.add_column("value", sqltype="TEXT")
