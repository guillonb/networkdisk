
# json module

```python
import json
dd = { None: 3, "null": 4 }
json.loads(json.dumps(dd)) == dd
```

# networkdisk

```python
import networkdisk as nd
ndG = nd.sqlite.Graph(lazy=True)
ndG[0][1]['color'] = "blue"
#⇒ the query is erroneous because the target is encoded twice.
```
